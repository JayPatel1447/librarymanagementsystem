﻿namespace LibrarySystem
{
    partial class AddProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this._txtAddress_Mem = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._txtEmail_Mem = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._txtPhone_Mem = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._txtLastName_Mem = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this._txtFirstName_Mem = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._txtMemberID1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_upload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(380, 182);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 98;
            this.label2.Text = "Choose profile photo";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(364, 21);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 149);
            this.pictureBox1.TabIndex = 112;
            this.pictureBox1.TabStop = false;
            // 
            // _txtAddress_Mem
            // 
            this._txtAddress_Mem.Location = new System.Drawing.Point(178, 182);
            this._txtAddress_Mem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtAddress_Mem.Name = "_txtAddress_Mem";
            this._txtAddress_Mem.Size = new System.Drawing.Size(127, 20);
            this._txtAddress_Mem.TabIndex = 111;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(80, 185);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 13);
            this.label22.TabIndex = 110;
            this.label22.Text = "Address:";
            // 
            // _txtEmail_Mem
            // 
            this._txtEmail_Mem.Location = new System.Drawing.Point(178, 150);
            this._txtEmail_Mem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtEmail_Mem.Name = "_txtEmail_Mem";
            this._txtEmail_Mem.Size = new System.Drawing.Size(127, 20);
            this._txtEmail_Mem.TabIndex = 109;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(80, 152);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 108;
            this.label23.Text = "Email:";
            // 
            // _txtPhone_Mem
            // 
            this._txtPhone_Mem.Location = new System.Drawing.Point(178, 117);
            this._txtPhone_Mem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtPhone_Mem.Name = "_txtPhone_Mem";
            this._txtPhone_Mem.Size = new System.Drawing.Size(127, 20);
            this._txtPhone_Mem.TabIndex = 107;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(80, 120);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 13);
            this.label24.TabIndex = 106;
            this.label24.Text = "Contact No:";
            // 
            // _txtLastName_Mem
            // 
            this._txtLastName_Mem.Location = new System.Drawing.Point(178, 84);
            this._txtLastName_Mem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtLastName_Mem.Name = "_txtLastName_Mem";
            this._txtLastName_Mem.Size = new System.Drawing.Size(127, 20);
            this._txtLastName_Mem.TabIndex = 105;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(80, 86);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(61, 13);
            this.label26.TabIndex = 104;
            this.label26.Text = "Last Name:";
            // 
            // _txtFirstName_Mem
            // 
            this._txtFirstName_Mem.Location = new System.Drawing.Point(178, 50);
            this._txtFirstName_Mem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtFirstName_Mem.Name = "_txtFirstName_Mem";
            this._txtFirstName_Mem.Size = new System.Drawing.Size(127, 20);
            this._txtFirstName_Mem.TabIndex = 103;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(80, 52);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(60, 13);
            this.label28.TabIndex = 102;
            this.label28.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 219);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 100;
            this.label3.Text = "Starting Date:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 97;
            this.label1.Text = "Member ID:";
            // 
            // _txtMemberID1
            // 
            this._txtMemberID1.Location = new System.Drawing.Point(178, 21);
            this._txtMemberID1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtMemberID1.Name = "_txtMemberID1";
            this._txtMemberID1.Size = new System.Drawing.Size(127, 20);
            this._txtMemberID1.TabIndex = 113;
            this._txtMemberID1.TextChanged += new System.EventHandler(this._AutoFilledData);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(178, 219);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(127, 20);
            this.textBox2.TabIndex = 114;
            // 
            // btn_update
            // 
            this.btn_update.Location = new System.Drawing.Point(136, 285);
            this.btn_update.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(61, 29);
            this.btn_update.TabIndex = 115;
            this.btn_update.Text = "Update";
            this.btn_update.UseVisualStyleBackColor = true;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(265, 285);
            this.btn_delete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(61, 29);
            this.btn_delete.TabIndex = 116;
            this.btn_delete.Text = "Clear";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this._ClearForm);
            // 
            // btn_upload
            // 
            this.btn_upload.Location = new System.Drawing.Point(396, 211);
            this.btn_upload.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(61, 29);
            this.btn_upload.TabIndex = 117;
            this.btn_upload.Text = "Upload";
            this.btn_upload.UseVisualStyleBackColor = true;
            // 
            // AddProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 335);
            this.Controls.Add(this.btn_upload);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this._txtMemberID1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this._txtAddress_Mem);
            this.Controls.Add(this.label22);
            this.Controls.Add(this._txtEmail_Mem);
            this.Controls.Add(this.label23);
            this.Controls.Add(this._txtPhone_Mem);
            this.Controls.Add(this.label24);
            this.Controls.Add(this._txtLastName_Mem);
            this.Controls.Add(this.label26);
            this.Controls.Add(this._txtFirstName_Mem);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "AddProfile";
            this.Text = "AddProfile";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox _txtAddress_Mem;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _txtEmail_Mem;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _txtPhone_Mem;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox _txtLastName_Mem;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox _txtFirstName_Mem;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _txtMemberID1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_upload;
    }
}