﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LibrarySystem
{
    public partial class IssueBook : Form
    {
        private SqlDataAdapter adapt;

        public IssueBook()
        {
            InitializeComponent();
            DisplayData();
            _txtAuthor.Enabled = false;
            _txtName.Enabled = false;
            _txtPrice.Enabled = false;
            _txtPublisher.Enabled = false;
            _Genre.Enabled = false;
            _AvailableComboBox.Enabled = false;

        }

        private void onClickExit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DisplayData()
        {
            string conStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Vishvakumar\\Downloads\\LibraryManagment.mdf;";
            SqlConnection con = new SqlConnection(conStr);
            con.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from AddBook where Available='Yes' ", con);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();


        }
    }
}
