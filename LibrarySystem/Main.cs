﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibrarySystem
{
    public partial class Main : Form
    {
        private Add_User user;
        private AddBook addBook;
        private IssueBook issueBook;
        private ReturnBook returnBook;
        private About abt;
        private AddProfile profileForm;
        private BookReport reportForm;
        public Main()
        {
            InitializeComponent();
            user = new Add_User();
            addBook = new AddBook();
            issueBook = new IssueBook();
            returnBook = new ReturnBook();
            abt = new About();
            profileForm = new AddProfile();
            reportForm = new BookReport();
        }

        private void onClickAddUser(object sender, EventArgs e)
        {
            user.ShowDialog();
        }

        private void onClickAddBook(object sender, EventArgs e)
        {
            addBook.ShowDialog();
        }

        private void onClickIssueBook(object sender, EventArgs e)
        {
            issueBook.ShowDialog();
        }

        private void onClickReturnBook(object sender, EventArgs e)
        {
            returnBook.ShowDialog();
        }

        private void onClickAbout(object sender, EventArgs e)
        {
            abt.ShowDialog();
        }

        private void onClickProfile(object sender, EventArgs e)
        {
            profileForm.ShowDialog();
        }

        private void onClickBookReports(object sender, EventArgs e)
        {
            reportForm.ShowDialog();
        }

        private void onClickSearch(object sender, EventArgs e)
        {
            reportForm.ShowDialog();
        }
    }
}
