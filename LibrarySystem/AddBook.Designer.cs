﻿namespace LibrarySystem
{
    partial class AddBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._Genre = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._AvailableComboBox = new System.Windows.Forms.ComboBox();
            this._txtPrice = new System.Windows.Forms.TextBox();
            this._txtPublisher = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._txtAuthor = new System.Windows.Forms.TextBox();
            this._txtName = new System.Windows.Forms.TextBox();
            this._txtBookId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_deleteBook = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_addBook = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._Genre);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._AvailableComboBox);
            this.groupBox1.Controls.Add(this._txtPrice);
            this.groupBox1.Controls.Add(this._txtPublisher);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._txtAuthor);
            this.groupBox1.Controls.Add(this._txtName);
            this.groupBox1.Controls.Add(this._txtBookId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(65, 30);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(434, 137);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Book Details";
            // 
            // _Genre
            // 
            this._Genre.Location = new System.Drawing.Point(130, 109);
            this._Genre.Margin = new System.Windows.Forms.Padding(2);
            this._Genre.Name = "_Genre";
            this._Genre.Size = new System.Drawing.Size(82, 20);
            this._Genre.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 109);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Category/Genre";
            // 
            // _AvailableComboBox
            // 
            this._AvailableComboBox.FormattingEnabled = true;
            this._AvailableComboBox.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this._AvailableComboBox.Location = new System.Drawing.Point(330, 83);
            this._AvailableComboBox.Margin = new System.Windows.Forms.Padding(2);
            this._AvailableComboBox.Name = "_AvailableComboBox";
            this._AvailableComboBox.Size = new System.Drawing.Size(82, 21);
            this._AvailableComboBox.TabIndex = 10;
            // 
            // _txtPrice
            // 
            this._txtPrice.Location = new System.Drawing.Point(330, 57);
            this._txtPrice.Margin = new System.Windows.Forms.Padding(2);
            this._txtPrice.Name = "_txtPrice";
            this._txtPrice.Size = new System.Drawing.Size(82, 20);
            this._txtPrice.TabIndex = 9;
            // 
            // _txtPublisher
            // 
            this._txtPublisher.Location = new System.Drawing.Point(330, 31);
            this._txtPublisher.Margin = new System.Windows.Forms.Padding(2);
            this._txtPublisher.Name = "_txtPublisher";
            this._txtPublisher.Size = new System.Drawing.Size(82, 20);
            this._txtPublisher.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 31);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Book ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 83);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Available";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(230, 57);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Price";
            // 
            // _txtAuthor
            // 
            this._txtAuthor.Location = new System.Drawing.Point(130, 83);
            this._txtAuthor.Margin = new System.Windows.Forms.Padding(2);
            this._txtAuthor.Name = "_txtAuthor";
            this._txtAuthor.Size = new System.Drawing.Size(82, 20);
            this._txtAuthor.TabIndex = 5;
            // 
            // _txtName
            // 
            this._txtName.Location = new System.Drawing.Point(130, 57);
            this._txtName.Margin = new System.Windows.Forms.Padding(2);
            this._txtName.Name = "_txtName";
            this._txtName.Size = new System.Drawing.Size(82, 20);
            this._txtName.TabIndex = 4;
            // 
            // _txtBookId
            // 
            this._txtBookId.Location = new System.Drawing.Point(130, 28);
            this._txtBookId.Margin = new System.Windows.Forms.Padding(2);
            this._txtBookId.Name = "_txtBookId";
            this._txtBookId.Size = new System.Drawing.Size(82, 20);
            this._txtBookId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(230, 31);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Publisher";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 83);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Author";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // btn_deleteBook
            // 
            this.btn_deleteBook.Location = new System.Drawing.Point(250, 316);
            this.btn_deleteBook.Margin = new System.Windows.Forms.Padding(2);
            this.btn_deleteBook.Name = "btn_deleteBook";
            this.btn_deleteBook.Size = new System.Drawing.Size(63, 31);
            this.btn_deleteBook.TabIndex = 7;
            this.btn_deleteBook.Text = "Delete";
            this.btn_deleteBook.UseVisualStyleBackColor = true;
            this.btn_deleteBook.Click += new System.EventHandler(this.onClickDeleteBook);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(375, 316);
            this.btn_exit.Margin = new System.Windows.Forms.Padding(2);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(63, 31);
            this.btn_exit.TabIndex = 6;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.onClickExit);
            // 
            // btn_addBook
            // 
            this.btn_addBook.Location = new System.Drawing.Point(125, 316);
            this.btn_addBook.Margin = new System.Windows.Forms.Padding(2);
            this.btn_addBook.Name = "btn_addBook";
            this.btn_addBook.Size = new System.Drawing.Size(63, 31);
            this.btn_addBook.TabIndex = 5;
            this.btn_addBook.Text = "Add Book";
            this.btn_addBook.UseVisualStyleBackColor = true;
            this.btn_addBook.Click += new System.EventHandler(this._btnAddBook);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(65, 188);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(434, 110);
            this.dataGridView1.TabIndex = 8;
            // 
            // AddBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 375);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_deleteBook);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_addBook);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AddBook";
            this.Text = "AddBook";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _AvailableComboBox;
        private System.Windows.Forms.TextBox _txtPrice;
        private System.Windows.Forms.TextBox _txtPublisher;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _txtAuthor;
        private System.Windows.Forms.TextBox _txtName;
        private System.Windows.Forms.TextBox _txtBookId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _Genre;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_deleteBook;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_addBook;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label10;
    }
}