﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LibrarySystem
{
    public partial class AddBook : Form
    {
        private SqlDataAdapter adapt;
        
        public AddBook()
        {
            InitializeComponent();
            _txtBookId.Enabled = false;
            DisplayData();
        }

        private void onClickExit(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void onClickDeleteBook(object sender, EventArgs e)
        {
            string conStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Vishvakumar\\Downloads\\LibraryManagment.mdf;";
            SqlConnection con = new SqlConnection(conStr);


            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(row.Index);
            }
        }
        private void DisplayData()
        {
            string conStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Vishvakumar\\Downloads\\LibraryManagment.mdf;";
            SqlConnection con = new SqlConnection(conStr);
            con.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from AddBook", con);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();


        }

        private void _btnAddBook(object sender, EventArgs e)
        {
            string conStr = "Data Source=(LocalDb)\\MSSQLLocalDb;Initial Catalog=LibraryManagment;Integrated Security=True";
            SqlConnection con = new SqlConnection(conStr);

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "INSERT INTO AddBook(Name,Author,Genre,Publisher,Price,Available) Values (@Name,@Author,@Genre,@Publisher,@Price,@Available)";
                    con.Open();
                    cmd.Connection = con;
                    
                    cmd.Parameters.AddWithValue("@Name", _txtName.Text);
                    cmd.Parameters.AddWithValue("@Author", _txtAuthor.Text);
                    cmd.Parameters.AddWithValue("@Genre", _Genre.Text);
                    cmd.Parameters.AddWithValue("@Publisher", _txtPublisher.Text);
                    cmd.Parameters.AddWithValue("@Price", _txtPrice.Text);
                    cmd.Parameters.AddWithValue("@Available", _AvailableComboBox.Text);


                    cmd.ExecuteNonQuery();
                    con.Close();

                    DialogResult result = MessageBox.Show("Record Inserted Successfully", "Exit", MessageBoxButtons.OK);


                    DisplayData();

                }
            }
            catch
            {
                MessageBox.Show("Error");
            }

        }
    }
}
