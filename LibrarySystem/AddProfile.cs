﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibrarySystem
{
    public partial class AddProfile : Form
    {
        private SqlDataAdapter adapt;

        public AddProfile()
        {
            InitializeComponent();
            
        }

        private void _AutoFilledData(object sender, EventArgs e)
        {
            string conStr = "Data Source=(LocalDb)\\MSSQLLocalDb;Initial Catalog=LibraryManagment;Integrated Security=True";
            SqlConnection con = new SqlConnection(conStr);
            
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "Select * from AddUser where MemberId ="+_txtMemberID1.Text;
                    con.Open();
                    cmd.Connection = con;
                    

                    SqlDataReader dReader;
                    dReader = cmd.ExecuteReader();

                    while (dReader.Read())
                    {
                        _txtFirstName_Mem.Text = dReader.GetString(1);
                        _txtLastName_Mem.Text = dReader.GetString(2);
                        _txtPhone_Mem.Text = Convert.ToString( dReader.GetInt32(4));
                        _txtEmail_Mem.Text = dReader.GetString(5);
                        _txtAddress_Mem.Text = dReader.GetString(6);
                        
                    }
                    dReader.Close();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    //DialogResult result = MessageBox.Show("Record Inserted Successfully", "Exit", MessageBoxButtons.OK);
                    

                   

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Something Went Wrong !!");
            }
        }

        private void _ClearForm(object sender, EventArgs e)
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                    
                }
            }
        }
    }
}
