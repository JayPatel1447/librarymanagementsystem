﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace LibrarySystem
{
    public partial class ReturnBook : Form
    {
        private SqlDataAdapter adapt;

        public ReturnBook()
        {
            InitializeComponent();
            DisplayData();
            _txtName.Enabled = false;
        }

        private void onClickExit(object sender, EventArgs e)
        {
            this.Close();
        }
        private void DisplayData()
        {
            string conStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Vishvakumar\\Downloads\\LibraryManagment.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection con = new SqlConnection(conStr);
            con.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from AddBook where Available='No' ", con);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();


        }
    }
}
