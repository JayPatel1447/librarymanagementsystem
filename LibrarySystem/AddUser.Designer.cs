﻿namespace LibrarySystem
{
    partial class Add_User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_addUser = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this._txtMemberId = new System.Windows.Forms.TextBox();
            this._txtAddress = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._txtEmail = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._txtContactNo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._txtLastName = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this._txtFirstName = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this._txtDate = new System.Windows.Forms.DateTimePicker();
            this._txtRole = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(340, 302);
            this.btn_delete.Margin = new System.Windows.Forms.Padding(2);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(63, 31);
            this.btn_delete.TabIndex = 11;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this._btnDelete);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(440, 302);
            this.btn_exit.Margin = new System.Windows.Forms.Padding(2);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(63, 31);
            this.btn_exit.TabIndex = 12;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.onClickExit);
            // 
            // btn_addUser
            // 
            this.btn_addUser.Location = new System.Drawing.Point(137, 302);
            this.btn_addUser.Margin = new System.Windows.Forms.Padding(2);
            this.btn_addUser.Name = "btn_addUser";
            this.btn_addUser.Size = new System.Drawing.Size(63, 31);
            this.btn_addUser.TabIndex = 9;
            this.btn_addUser.Text = "Add New";
            this.btn_addUser.UseVisualStyleBackColor = true;
            this.btn_addUser.Click += new System.EventHandler(this._btnAddNew);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(238, 302);
            this.btn_save.Margin = new System.Windows.Forms.Padding(2);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(63, 31);
            this.btn_save.TabIndex = 10;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this._btnUpdate);
            // 
            // _txtMemberId
            // 
            this._txtMemberId.Enabled = false;
            this._txtMemberId.Location = new System.Drawing.Point(194, 30);
            this._txtMemberId.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtMemberId.Name = "_txtMemberId";
            this._txtMemberId.Size = new System.Drawing.Size(127, 20);
            this._txtMemberId.TabIndex = 1;
            // 
            // _txtAddress
            // 
            this._txtAddress.Location = new System.Drawing.Point(465, 92);
            this._txtAddress.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtAddress.Name = "_txtAddress";
            this._txtAddress.Size = new System.Drawing.Size(127, 20);
            this._txtAddress.TabIndex = 7;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(368, 95);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 13);
            this.label22.TabIndex = 125;
            this.label22.Text = "Address:";
            // 
            // _txtEmail
            // 
            this._txtEmail.Location = new System.Drawing.Point(465, 60);
            this._txtEmail.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtEmail.Name = "_txtEmail";
            this._txtEmail.Size = new System.Drawing.Size(127, 20);
            this._txtEmail.TabIndex = 6;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(368, 62);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 123;
            this.label23.Text = "Email:";
            // 
            // _txtContactNo
            // 
            this._txtContactNo.Location = new System.Drawing.Point(465, 27);
            this._txtContactNo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtContactNo.Name = "_txtContactNo";
            this._txtContactNo.Size = new System.Drawing.Size(127, 20);
            this._txtContactNo.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(368, 30);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 13);
            this.label24.TabIndex = 121;
            this.label24.Text = "Contact No:";
            // 
            // _txtLastName
            // 
            this._txtLastName.Location = new System.Drawing.Point(194, 93);
            this._txtLastName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtLastName.Name = "_txtLastName";
            this._txtLastName.Size = new System.Drawing.Size(127, 20);
            this._txtLastName.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(96, 95);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(61, 13);
            this.label26.TabIndex = 119;
            this.label26.Text = "Last Name:";
            // 
            // _txtFirstName
            // 
            this._txtFirstName.Location = new System.Drawing.Point(194, 59);
            this._txtFirstName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this._txtFirstName.Name = "_txtFirstName";
            this._txtFirstName.Size = new System.Drawing.Size(127, 20);
            this._txtFirstName.TabIndex = 2;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(96, 61);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(60, 13);
            this.label28.TabIndex = 117;
            this.label28.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(368, 129);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 116;
            this.label3.Text = "Starting Date:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(96, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 115;
            this.label1.Text = "Member ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(97, 133);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 129;
            this.label2.Text = "Role:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(55, 163);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(537, 134);
            this.dataGridView1.TabIndex = 131;
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridView1_RowHeaderMouseClick);
            // 
            // _txtDate
            // 
            this._txtDate.Location = new System.Drawing.Point(465, 127);
            this._txtDate.Name = "_txtDate";
            this._txtDate.Size = new System.Drawing.Size(127, 20);
            this._txtDate.TabIndex = 132;
            // 
            // _txtRole
            // 
            this._txtRole.FormattingEnabled = true;
            this._txtRole.Items.AddRange(new object[] {
            "Librarian",
            "Member"});
            this._txtRole.Location = new System.Drawing.Point(194, 129);
            this._txtRole.Name = "_txtRole";
            this._txtRole.Size = new System.Drawing.Size(121, 21);
            this._txtRole.TabIndex = 133;
            // 
            // Add_User
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 361);
            this.Controls.Add(this._txtRole);
            this.Controls.Add(this._txtDate);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._txtMemberId);
            this.Controls.Add(this._txtAddress);
            this.Controls.Add(this.label22);
            this.Controls.Add(this._txtEmail);
            this.Controls.Add(this.label23);
            this.Controls.Add(this._txtContactNo);
            this.Controls.Add(this.label24);
            this.Controls.Add(this._txtLastName);
            this.Controls.Add(this.label26);
            this.Controls.Add(this._txtFirstName);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_addUser);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Add_User";
            this.Text = "Add_User";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_addUser;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.TextBox _txtMemberId;
        private System.Windows.Forms.TextBox _txtAddress;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _txtEmail;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _txtContactNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox _txtLastName;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox _txtFirstName;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker _txtDate;
        private System.Windows.Forms.ComboBox _txtRole;
    }
}