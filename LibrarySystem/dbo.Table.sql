﻿CREATE TABLE [dbo].[AddUser] (
    [MemberID]    INT    IDENTITY (1, 1)       NOT NULL,
    [FirstName]   VARCHAR (10) NOT NULL,
    [LastName]    VARCHAR (10) NULL,
    [Role]        VARCHAR (3) NULL,
    [ContactInfo] INT          NULL,
    [Email]       VARCHAR (50) NULL,
    [Address]     VARCHAR (20) NULL,
    [Date]        Date          NULL,
    PRIMARY KEY CLUSTERED ([MemberID] ASC)
);

