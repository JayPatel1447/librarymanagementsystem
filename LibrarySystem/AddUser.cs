﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibrarySystem
{
    public partial class Add_User : Form
    {
        public Add_User()
        {
            InitializeComponent();
            DisplayData();
            ClearData();
            _txtMemberId.Enabled = false;
        }
        SqlDataAdapter adapt;
       

        private void _btnAddNew(object sender, EventArgs e)
        {
            string conStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Vishvakumar\\Downloads\\LibraryManagment.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection con = new SqlConnection(conStr);

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    
                    cmd.CommandText = "INSERT INTO AddUser(FirstName,LastName,Role,ContactInfo,Email,Address,Date) Values (@FirstName,@LastName,@Role,@ContactInfo,@Email,@Address,@Date)";
                    con.Open();
                    cmd.Connection = con;
                    
                    cmd.Parameters.AddWithValue("@FirstName", _txtFirstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", _txtLastName.Text);
                    cmd.Parameters.AddWithValue("@Role", _txtRole.SelectedItem);
                    cmd.Parameters.AddWithValue("@ContactInfo", _txtContactNo.Text);
                    cmd.Parameters.AddWithValue("@Email", _txtEmail.Text);
                    cmd.Parameters.AddWithValue("@Address", _txtAddress.Text);
                    cmd.Parameters.AddWithValue("@Date", _txtDate.Value.ToShortDateString());

                    cmd.ExecuteNonQuery();
                    con.Close();
                   
                    DialogResult result = MessageBox.Show("Record Inserted Successfully", "Exit", MessageBoxButtons.OK);
                    

                    DisplayData();
                    
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Error");
            }
            
        }

        private void DisplayData()
        {
            string conStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Vishvakumar\\Downloads\\LibraryManagment.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection con = new SqlConnection(conStr);
            con.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from AddUser",con);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();


        }
        private void onClickExit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void _btnDelete(object sender, EventArgs e)
        {
            string conStr = "Data Source=(LocalDb)\\MSSQLLocalDb;Initial Catalog=LibraryManagment;Integrated Security=True";
            SqlConnection con = new SqlConnection(conStr);

            
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(row.Index);
            }
        }

        private void ClearData()
        {

            _txtMemberId.Text = "";
            _txtFirstName.Text = "";
            _txtLastName.Text = "";
            _txtAddress.Text = "";
            _txtContactNo.Text = "";
            _txtEmail.Text = "";
            _txtDate.Text = "";
            _txtRole.Text = "";
            
            
        }

       

        //private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    //int rowIndex = dataGridView1.CurrentCell.RowIndex;
        //    //DataGridViewRow row = dataGridView1.Rows[rowIndex];
        //    //_txtMemberId.Text = row.Cells[0].Value.ToString();
        //    //_txtFirstName.Text = row.Cells[1].Value.ToString();
        //    //_txtLastName.Text = row.Cells[2].Value.ToString();
        //    //_txtRole.Text = row.Cells[3].Value.ToString();
        //    //_txtContactNo.Text = row.Cells[4].Value.ToString();
        //    //_txtEmail.Text = row.Cells[5].Value.ToString();
        //    //_txtAddress.Text = row.Cells[6].Value.ToString();
        //    //_txtDate.Text = row.Cells[7].Value;
           
         
        //}

        private void DataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIndex = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[rowIndex];
            _txtMemberId.Text = row.Cells[0].Value.ToString();
            _txtFirstName.Text = row.Cells[1].Value.ToString();
            _txtLastName.Text = row.Cells[2].Value.ToString();
            _txtRole.Text = row.Cells[3].Value.ToString();
            _txtContactNo.Text = row.Cells[4].Value.ToString();
            _txtEmail.Text = row.Cells[5].Value.ToString();
            _txtAddress.Text = row.Cells[6].Value.ToString();
            _txtDate.Value = Convert.ToDateTime(row.Cells[7].Value);
            btn_addUser.Enabled = false;
        }

        private void _btnUpdate(object sender, EventArgs e)
        {
            string conStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Vishvakumar\\Downloads\\LibraryManagment.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection con = new SqlConnection(conStr);

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "UPDATE AddUser set FirstName=@FirstName,LastName=@LastName,Role=@Role,ContactInfo=@ContactInfo,Email=@Email,Address=@Address,Date=@Date where MemberID="+_txtMemberId.Text;
                    con.Open();
                    cmd.Connection = con;

                    cmd.Parameters.AddWithValue("@FirstName", _txtFirstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", _txtLastName.Text);
                    cmd.Parameters.AddWithValue("@Role", _txtRole.SelectedItem);
                    cmd.Parameters.AddWithValue("@ContactInfo", _txtContactNo.Text);
                    cmd.Parameters.AddWithValue("@Email", _txtEmail.Text);
                    cmd.Parameters.AddWithValue("@Address", _txtAddress.Text);
                    cmd.Parameters.AddWithValue("@Date", _txtDate.Value.ToShortDateString());

                    cmd.ExecuteNonQuery();
                    con.Close();

                    DialogResult result = MessageBox.Show("Record Inserted Successfully", "Exit", MessageBoxButtons.OK);


                    DisplayData();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Error");
            }
        }
    }
}
