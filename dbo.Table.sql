﻿CREATE TABLE [dbo].[AddUser] (
    [MemberID]    INT          NOT NULL,
    [FirstName]   VARCHAR (10) NOT NULL,
    [LastName]    VARCHAR (10) NULL,
    [Role]        VARCHAR (10) NULL,
    [ContactInfo] INT          NULL,
    [Email]       VARCHAR (50) NULL,
    [Address]     VARCHAR (20) NULL,
    [Date]        INT         NULL,
    PRIMARY KEY CLUSTERED ([MemberID] ASC)
);

