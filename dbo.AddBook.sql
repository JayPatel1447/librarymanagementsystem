﻿CREATE TABLE [dbo].[AddBook]
(
	[BookID] INT Identity(1,1) NOT NULL  PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL, 
    [Author] VARCHAR(50) NOT NULL, 
    [Genre] VARCHAR(50) NOT NULL, 
    [Publisher] VARCHAR(50) NOT NULL, 
    [Price] FLOAT NOT NULL, 
    [Available] VARCHAR(3) NULL
)
